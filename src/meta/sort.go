package meta

import (
	"net"
	"time"
)

const baseFormat = "YYYY-MM-DD hh:mm:ss"

type ByCreateTime []FileMeta

func (arr ByCreateTime) Len() int {
	return len(arr)
}

func (arr ByCreateTime) Swap(lhs, rhs int) {
	arr[lhs], arr[rhs] = arr[rhs], arr[lhs]
}
