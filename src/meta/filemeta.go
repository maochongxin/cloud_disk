package meta

import (
	"sort"
)

type FileMeta struct {
	FileName 	string
	FilePath 	string
	FileSize 	int64
	Hash 		string
	CreateTime 	string
}

var fileMetas map[string]FileMeta

func init()  {
	fileMetas = make(map[string]FileMeta)
}

func UpdateFileMeta(filemeta FileMeta)  {
	fileMetas[filemeta.FileName] = filemeta
}

func GetFileMeta(filename string) FileMeta  {
	return fileMetas[filename]
}

func RemoveFileMeta(filename string)  {
	delete(fileMetas, filename)
}

func GetLastFileMetas(count int) []FileMeta  {
	len := len(fileMetas)
	if count > len {
		count = len
	}
	FileMetaArray := make([]FileMeta, len)
	for _, v := range fileMetas {
		FileMetaArray = append(FileMetaArray, v)
	}

	sort.Sort(ByCreateTime(FileMetaArray))

	return FileMetaArray[:count]
}
