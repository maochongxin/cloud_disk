package handler

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"../meta"
	"net/http"
	"os"
	"strconv"
	"time"
	"../util"
)



const baseFormat = "YYYY-MM-DD hh:mm:ss"
const dirPath = "~/Desktop"

func UploadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		data, err := ioutil.ReadFile("")
		if err != nil {
			fmt.Println("Read file index.html error")
			return
		}
		io.WriteString(w, string(data))
		fmt.Println("Read file index.html")
	} else if r.Method == "POST" {
		// get the file
		file, handler, err := r.FormFile("file")
		if err != nil {
			fmt.Println("Failed to upload file")
			return
		}

		defer file.Close()
		// the store path
		filePath := dirPath + handler.Filename
		// create fimeMeta to source file meta
		fileMeta := meta.FileMeta {
			FileName: handler.Filename,
			FilePath: filePath,
			CreateTime: time.Now().Format(baseFormat),
		}

		realFile, err := os.Create(filePath)
		if err != nil {
			fmt.Println("Failed to create file: " + handler.Filename)
			return
		}

		defer realFile.Close()

		size, err := io.Copy(realFile, file)
		if err != nil {
			fmt.Println("Failed move file to newFile")
			return
		}

		// store size
		fileMeta.FileSize = size
		// store hash
		realFile.Seek(0, 0);
		fileMeta.Hash = util.FileMD5(realFile);
		fmt.Println(fileMeta)
		// store fileMeta
		meta.UpdateFileMeta(fileMeta)
		fmt.Println(handler.Filename + " Upload success")
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

func SucHandler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Upload success")
}

func GetFileMetaHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	fileName := r.Form["filename"][0]
	fileMeta := meta.GetFileMeta(fileName)
	fmt.Println("get filename: ", fileName, " fileMeta: ", fileMeta)
	data, err := json.Marshal(fileMeta)
	if err != nil {
		fmt.Println("GetFileMetaHandler: parse fileMeta error")
		return
	}

	// tranfer json to client
	w.Write(data)
}

func QueryMultiHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	count, _ := strconv.Atoi(r.Form.Get("limit"))
	fmt.Println("QueryMultiHandler: count", count)
	fileMetas := meta.GetLastFileMetas(count)
	data, err := json.Marshal(fileMetas)
	if err != nil {
		fmt.Println("QueryMultiHandler: parse fileMetas error")
		return
	}

	w.Write(data)
}

func FileDownloadHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fileName := r.Form.Get("filename")
	fileMeta := meta.GetFileMeta(fileName)
	fmt.Println("FileDownloadHandler: fileMeta: ", fileMeta)

	file, err := os.Open(fileMeta.FilePath)
	if err != nil {
		fmt.Println("FileDownloadHandler: can not fine the file: ", fileMeta.FilePath)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println("FileDownloadHandler: can not read the file: ", fileMeta.FilePath)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/octect-stream")
	w.Header().Set("content-disposition", "attachment; filename=\"" + fileMeta.FileName + "\"")

	w.Write(data)
}

func FileDeleteHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fileName := r.Form.Get("filename")
	fmt.Println("FileDeleteHandler: filename: ", fileName)
	fileMeta := meta.GetFileMeta(fileName)
	fmt.Println("FileDeleteHandler: fileMeta: ", fileMeta)
	os.Remove(fileMeta.FilePath)
	meta.RemoveFileMeta(fileName)
	fmt.Println("FileDeleteHandler: delete file: ", fileMeta.FilePath, " ok")

	//set ok
	w.WriteHeader(http.StatusOK)
}

func FileMetaUpdateHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fileName := r.Form.Get("filename")
	newFileName := r.Form.Get("newfilename")

	fmt.Println("FileMetaUpdateHandler: filename: ", fileName, "newfilename: ", newFileName)

	fileMeta := meta.GetFileMeta(fileName)
	fmt.Println("FileMetaUpdateHandler: old fileMeta: ", fileMeta)

	// remove old file meta
	meta.RemoveFileMeta(fileName)
	// store new fileMeta
	fileMeta.FileName = newFileName
	meta.UpdateFileMeta(fileMeta)
	fmt.Println("FileMetaUpdateHandler: new fileMeta: ", FileMeta)

	// set ok
	w.WriteHeader(http.StatusOK)
}


