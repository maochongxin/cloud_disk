package main

import (
	"encoding/binary"
	"fmt"
	"./handler"
	"net/http"
)

func main() {
	const listenerPort = ":80"
 	http.HandleFunc("/file/upload", handler.UploadHandler)
 	http.HandlerFunc("/file/upload/suc", handler.SucHandler)
	http.HandlerFunc("/file/meta", handler.GetFileMetaHandler)
	http.HandlerFunc("/file/query", handler.QueryMultiHandler)
	http.HandlerFunc("/file/down", handler.FileDownloadHandler)
	http.HandlerFunc("/file/del", handler.FileDownloadHandler)
	http.HandlerFunc("/file/update", handler.FileMetaUpdateHandler)

	err := http.ListenAndServe(listenerPort, nil)
	if err != nil {
		fmt.Println("server start filed err: " + err.Error() + "\n")
		return
	}
}
